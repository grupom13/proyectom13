# Proyecto M13 Arduino

Este es el repositorio del proyecto de Arduino de M13

![Swagger API](http://oi65.tinypic.com/dr5r2g.jpg)

![Swagger API](http://oi67.tinypic.com/29oqq06.jpg)

## Instalación

Para instalar este proyecto se necesita tener un dispositivo Android para probar los datos, un compilador de Java para ejecutar la API y un servidor con una base de datos MySQL donde se guardarán estos.

## Uso

Para utilizar este proyecto se puede hacer desde la parte de la API ([Swagger](http://localhost:8080/swagger-ui.html)) y/o desde el cliente Android. Para ello el servidor donde esté la base de datos tiene que estar encendido y la aplicación API también.

## Base de datos
```mysql
create database projecte;

create table projecte.dades(
id integer primary key auto_increment,
temperatura float not null,
humedad float not null,
aula int not null,
data DATETIME not null
);
```

## Configuración

Hay que modificar las direcciones IP por las del servidor API en la Aplicación Android y en el código de Arduino y de la API por donde se encuentre el servidor de la BD.

## Participantes
Mario Sangorrín

Robin Morató

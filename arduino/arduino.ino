#include <DHT.h>
#include <Ethernet.h>
#include <SPI.h>

byte mac[] = { 0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x01 }; // RESERVED MAC ADDRESS
EthernetClient client;

#define DHTPIN 2 // SENSOR PIN
#define AULA 303 // CONSTANTE AULA
#define DHTTYPE DHT11 // SENSOR TYPE - THE ADAFRUIT LIBRARY OFFERS SUPPORT FOR MORE MODELS
DHT dht(DHTPIN, DHTTYPE);

long previousMillis = 0;
unsigned long currentMillis = 0;
long interval = 250000; // READING INTERVAL

float t = 0;  // TEMPERATURE VAR
float h = 0;  // HUMIDITY VAR
String data;

void setup() { 
  Serial.begin(115200);
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP"); 
  }
  dht.begin(); 
  delay(10000); // GIVE THE SENSOR SOME TIME TO START
}

void loop(){
  h = dht.readHumidity();
  t = dht.readTemperature();
  data = "temp=";
  data += t;
  data += "&hum=";
  data += h;
  data += "&aula=";
  data += AULA;
  Serial.println(data);
  if (client.connect("192.168.111.107",80)) { // REPLACE WITH YOUR SERVER ADDRESS
    client.println("POST /add.php HTTP/1.1"); 
    client.println("Host: 192.168.111.107"); // SERVER ADDRESS HERE TOO
    client.println("Content-Type: application/x-www-form-urlencoded"); 
    client.print("Content-Length: "); 
    client.println(data.length()); 
    client.println(); 
    client.print(data);
    Serial.println("Dato enviado");
  } else {
    Serial.println("Error no se puede conectar al servidor");
  }
  if (client.connected()) { 
    client.stop();  // DISCONNECT FROM THE SERVER
  }
  data = "";
  delay(10000); // WAIT FIVE MINUTES BEFORE SENDING AGAIN
}

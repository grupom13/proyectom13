package i.java.temperature.problem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

//Afegit el componentScan
@SpringBootApplication
@ComponentScan(basePackageClasses = {IJavaTemperatureProblemApplication.class})
public class IJavaTemperatureProblemApplication {

    public static void main(String[] args) {
        SpringApplication.run(IJavaTemperatureProblemApplication.class, args);
    }

}

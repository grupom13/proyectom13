package i.java.temperature.problem.dao;

import org.springframework.data.repository.CrudRepository;

import i.java.temperature.problem.domini.Registre;
import java.util.Date;
import java.util.List;

public interface RepositoriDeRegistresSpring extends CrudRepository<Registre, Long> {

    //Para buscar por aula o por cualquier otro campo tiene que ir con un formato caseSensitive
    //findbyaula !=  findByAula
    //Y también hay otros tipos de búsqueda -> findByEdatGreaterThan
    //See https://docs.spring.io/spring-data/jpa/docs/current/reference/html/ for defining new methods
    //Thanks -> https://stackoverflow.com/questions/41795544/crudrepository-custom-method-implementation
    List<Registre> findByAula(int aula);
    List<Registre> findByAulaAndDataAfterAndDataBefore(int aula, Date data2, Date data3);
    List<Registre> findByDataAfterAndDataBefore(Date data2, Date data3);
}

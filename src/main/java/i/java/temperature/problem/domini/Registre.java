package i.java.temperature.problem.domini;

import io.swagger.annotations.ApiModelProperty;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//Entity -> La tabla de la BD con el modelo de la clase actual
@Entity(name = "dades")
public class Registre {

    @ApiModelProperty(notes = "Registre generat automàticament a la BD")
    @Id
    //GeneratedValue en identity y en la base de datos el id ha de ser AutoIncrement (AI) -> Auto da errores
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ApiModelProperty(notes = "La temperatura")
    private float temperatura;
    @ApiModelProperty(notes = "La humitat")
    private float humedad;
    @ApiModelProperty(notes = "Nombre de l'aula")
    private int aula;
    @ApiModelProperty(notes = "Data la qual s'envia el registre")
    private Date data;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getAula() {
        return aula;
    }

    public void setAula(int aula) {
        this.aula = aula;
    }

    public float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(float temperatura) {
        this.temperatura = temperatura;
    }

    public float getHumedad() {
        return humedad;
    }

    public void setHumedad(float humedad) {
        this.humedad = humedad;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

}

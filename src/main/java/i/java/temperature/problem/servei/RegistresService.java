package i.java.temperature.problem.servei;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import i.java.temperature.problem.dao.RepositoriDeRegistresSpring;
import i.java.temperature.problem.domini.Registre;
import java.util.Date;
import org.springframework.stereotype.Service;
 
//Declarar service para declararlo como service en spring
@Service
public class RegistresService implements ServeiRegistresInterface {

    //Las interfaces tienen que estar definidas con AutoWired para la correcta inyección
    @Autowired()
    private RepositoriDeRegistresSpring registres;
    
    @Override
    public List<Registre> getRegistres() {
        return (List<Registre>) registres.findAll();
    }
    
    @Override
    public void addRegistre(Registre r) {
        registres.save(r);
    }
    
    @Override
    public List<Registre> getRegistreAula(int aula) {
        return registres.findByAula(aula);
    }
    
    @Override
    public List<Registre> getRegistreAula(int aula, Date data) {
        Date data2 = new Date(data.getYear(), data.getMonth(), (data.getDate() + 1));
        return registres.findByAulaAndDataAfterAndDataBefore(aula, data, data2);
    }
    
    @Override
    public List<Registre> getRegistreData(Date data) {
        Date data2 = new Date(data.getYear(), data.getMonth(), (data.getDate() + 1));
        return registres.findByDataAfterAndDataBefore(data, data2);
    }
    
    @Override
    public List<Registre> getRegistreDates(Date data, Date data2) {
        return registres.findByDataAfterAndDataBefore(data, data2);
    }
    
    @Override
    public List<Registre> getRegistreAulaDates(int aula, Date data, Date data2) {
        return registres.findByAulaAndDataAfterAndDataBefore(aula, data, data2);
    }
    
    @Override
    public void updateRegistre(Long id, Registre registre) throws Exception {
        Registre actual = registres.findById(id).orElseThrow(() -> new Exception("Registre no existent amb aquesta id"));
        actual.setAula(registre.getAula());
        actual.setData(registre.getData());
        actual.setHumedad(registre.getHumedad());
        actual.setTemperatura(registre.getTemperatura());
        registres.save(actual);
    }
    
    @Override
    public void deleteRegistre(Long id) {
        registres.deleteById(id);
    }

}

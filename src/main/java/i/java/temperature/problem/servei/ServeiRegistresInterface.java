package i.java.temperature.problem.servei;

import java.util.List;

import i.java.temperature.problem.domini.Registre;
import java.util.Date;

public interface ServeiRegistresInterface {

    public List<Registre> getRegistres();

    public List<Registre> getRegistreAula(int aula);
    
    public List<Registre> getRegistreAula(int aula, Date data);
    
    public List<Registre> getRegistreData(Date data);
    
    public List<Registre> getRegistreDates(Date data, Date data2);
    
    public List<Registre> getRegistreAulaDates(int aula, Date data, Date data2);

    public void addRegistre(Registre r);

    public void updateRegistre(Long id, Registre registre) throws Exception;

    public void deleteRegistre(Long id);
}
